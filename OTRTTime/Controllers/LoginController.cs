﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using OTRTTime.Models;


namespace OTRTTime.Controllers
{
    [Produces("application/json")]
    [Route("api/Login")]
    public class LoginController : Controller
    {
        private readonly OTRTTime_TestContext _context;

        public LoginController(OTRTTime_TestContext context)
        {
            _context = context;
        }


        [HttpGet("[action]")]
        public string TestGet()
        {
            using (var ctx = _context)
            {
                var user = ctx.User.FirstOrDefault();

                return "Ok";
            }
        }

        [HttpPost]
        public IActionResult AddUser()
        {
            using (var ctx = _context)
            {
                var user = new User
                {
                    Name = "Bob",
                    Email = "bob@otrt.co.th",
                    Password = "Welcome1",
                    CreatedDate = DateTime.Now,
                    UpdatedDate = DateTime.Now
                };

                ctx.User.Add(user);
                ctx.SaveChanges();

                return Ok();
            }
        }
    }
}