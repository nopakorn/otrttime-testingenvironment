﻿import { Component, Inject } from '@angular/core';
import { Http } from '@angular/http';

@Component({
    selector: 'login',
    templateUrl: './login.component.html'
})
export class LoginComponent {
    public loginModel: LoginModel;
    public token: string;
    public http: Http;
    public baseURL: string;
    
    constructor(http: Http, @Inject('BASE_URL') baseUrl: string) {
        this.http = http;
        this.baseURL = baseUrl;
    }

    public Login() {
        //this.http.get(this.baseURL + 'api/SampleData/WeatherForecasts').subscribe(result => {
            
        //}, error => console.error(error));

        this.http.post(this.baseURL + 'api/Login/AddUser', null).subscribe(result => {

        }, error => console.error(error));
    }
}

interface LoginModel {
    email: string;
    password: string;
}